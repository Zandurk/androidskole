package zandurk.android_lessons;

/**
 * Created by Alexander on 17-02-2016.
 */
public enum State {
    RUNNING,
    PAUSED,
    RESUMED,
    DISPOSED
}
