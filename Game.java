package zandurk.android_lessons;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 17-02-2016.
 */
public abstract class Game extends Activity implements Runnable

{

    private Thread mainLoopThread;
    private State state = State.PAUSED;
    private List<State> stateChanges = new ArrayList<>();
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Screen screen;

    public abstract Screen createStartScreen();
    public void setScreen(Screen screen)
    {
        if (this.screen != null) this.screen.dispose();
        this.screen = screen;
    }


    public Bitmap loadBitMap(String fileName)
    {
        return null;
    }
/*
    public Music loadMusic(String fileName)
    {
        return null;
    }

    public Sound loadSound(String fileName)
    {
     return null;
    }
*/
    public void clearFrameBuffer(int color)
    {
    }

    public int getFrameBufferWidth(){
        return 0;
    }

    public int getFrameBufferHeight(){
        return 0;
    }

    public void drawBitMap(Bitmap bitmap, int x, int y)
    {
    }

    public void drawBitMap(Bitmap bitmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight)
    {

    }

    public boolean isKeyPressed(int keyCode)
    {
        return false;
    }

    public boolean isTouchDown(int pointer) //Pointer -- Hvor mange fingrer rører ved skærmen?
    {
    return false;
    }

    public int getTouchX(int pointer){
        return 0;
    }

    public int getTouchY(int pointer){
        return 0;
    }
/*
    public List<KeyEvent> getKeyEvents()
    {
     return null;
    }
*/
    public float[] getAccelormeter(){
        return null;
    }


    public void run(){
        while(true){
            synchronized (stateChanges){
                for (int i = 0; i < stateChanges.size(); i++) {
                    state = stateChanges.get(i);
                    if (state == State.DISPOSED){
                        Log.d("Game","State is disposed");
                    }
                    else if (state == State.PAUSED){
                        Log.d("Game","State is paused");
                    }
                    else if (state == State.RUNNING){
                        Log.d("Game","State is running");
                    }
                    else if (state == State.RESUMED){
                        state = State.RUNNING;
                        Log.d("Game","State is resumed");
                    }
                }
            stateChanges.clear();
            }
        if (state == State.RUNNING){
            if (!surfaceHolder.getSurface().isValid()) continue;
            Canvas canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.RED);
            surfaceHolder.unlockCanvasAndPost(canvas);

        }
        }
    }

    protected void OnCreate(Bundle instanceBundle){
        super.onCreate(instanceBundle);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        surfaceView = new SurfaceView(this);
        surfaceHolder = surfaceView.getHolder();
        screen = createStartScreen();

    }

    public void onPause(){
        super.onPause();
        synchronized (stateChanges){
            if (isFinishing()){
                stateChanges.add(stateChanges.size(), State.DISPOSED);
            }
            else{
                stateChanges.add(stateChanges.size(), State.PAUSED);
            }

        }
        try{
            mainLoopThread.join();
        }catch (InterruptedException e){

        }
    }

    public void onResume(){
        super.onResume();
        mainLoopThread = new Thread(this);
        mainLoopThread.start();
        synchronized (stateChanges){
            stateChanges.add(stateChanges.size(), State.RESUMED);
        }
    }


}
