package zandurk.android_lessons;

/**
 * Created by Alexander on 17-02-2016.
 */
public class SimpleGame extends Game
{

    @Override
    public Screen createStartScreen() {
        return new SimpleScreen(this);
    }
}
