package zandurk.android_lessons;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Alexander on 17-02-2016.
 */
public class SimpleScreen extends Screen
{
    Bitmap bitmap;
    int x = 0;
    int y = 0;


    public SimpleScreen(Game game){
        super(game);
        bitmap = game.loadBitMap("bob.png");
    }

    @Override
    public void update(float deltaTime) {
    if (game.isTouchDown(0)){
        x = game.getTouchX(0);
        y = game.getTouchY(0);
    }
        game.clearFrameBuffer(Color.RED);
        game.drawBitMap(bitmap, x, y);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
